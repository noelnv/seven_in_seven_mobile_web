FROM ruby:2.3

EXPOSE 4567


RUN apt-get update && apt-get install -y nodejs
RUN apt-get update && apt-get install -y npm

RUN mkdir /WebApp
WORKDIR /WebApp

COPY Gemfile Gemfile
COPY app.rb app.rb

COPY app/ app/


WORKDIR /WebApp/app

RUN npm install

WORKDIR /WebApp
RUN bundle install

CMD ruby app.rb