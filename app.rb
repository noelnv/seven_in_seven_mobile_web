require 'sinatra'
set :bind, '0.0.0.0'
set :port, 4567
set :public_folder, File.dirname(__FILE__) + '/app'

get "/" do
	send_file "app/index.html"
end

get "/manifest.appcache" do
	send_file "app/manifest.appcache", :type => "text/cache-manifest"
end